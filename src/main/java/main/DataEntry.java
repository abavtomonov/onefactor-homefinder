package main;

import java.io.Serializable;
import java.util.Date;

public class DataEntry implements Serializable {
    private int id;
    private Date date;
    private double x;
    private double y;

    public DataEntry(String s) {
        String[] split = s.split(",");
        int id = Integer.parseInt(split[0]);
        setId(id);
        setDate(new Date(1000 * Long.parseLong(split[1])));
        setX(Double.parseDouble(split[2]));
        setY(Double.parseDouble(split[3]));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "DataEntry{" +
                "id=" + id +
                ", date=" + date +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}

