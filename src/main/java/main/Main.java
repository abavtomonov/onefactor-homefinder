package main;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.clustering.BisectingKMeans;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Main {

    private static final TimeZone MOSCOW_TIMEZONE = TimeZone.getTimeZone("Europe/Moscow");
    private static final String INPUT_PATH = "";
    private static final int HOURS_FROM = 23;
    private static final int HOURS_TO = 4;
    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        Logger.getLogger("org").setLevel(Level.ERROR);
        Logger.getLogger("akka").setLevel(Level.ERROR);

        JavaSparkContext sc = createSparkContext();
        String inputPath = args.length > 0 ? args[0] : INPUT_PATH;
        detectHomeByInputFile(sc, inputPath);
        sc.close();
    }

    private static void detectHomeByInputFile(JavaSparkContext sc, String inputPath) {
        JavaRDD<String> rdd = sc.textFile(inputPath);
        String first = rdd.first();
        JavaRDD<String> dataRdd = rdd.filter(v1 -> !v1.equals(first));
        detectHome(dataRdd);
    }

    private static void detectHome(JavaRDD<String> dataRdd) {
        JavaRDD<DataEntry> filteredByTimeRdd = dataRdd
                .map(DataEntry::new)
                .filter(record -> isItNightAtMoscowAt(record.getDate()))
                .cache();

        List<Integer> ids = filteredByTimeRdd.map(DataEntry::getId).distinct().collect();

        for (Integer id : ids) {
            JavaRDD<DataEntry> filter = filteredByTimeRdd.filter(v1 -> v1.getId() == id);
            JavaRDD<Vector> map = filter.map(v1 -> Vectors.dense(v1.getX(), v1.getY())).cache();
            logger.info("Device id : " + id + " Home coordinates : " + detectCenter(map));
        }
    }

    public static Vector detectCenter(JavaRDD<Vector> coordinates) {
        BisectingKMeans bkm = new BisectingKMeans().setK(1);
        BisectingKMeansModel model = bkm.run(coordinates);
        return model.clusterCenters()[0];
    }

    public static Boolean isItNightAtMoscowAt(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(MOSCOW_TIMEZONE);
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour >= HOURS_FROM || hour <= HOURS_TO;
    }

    public static JavaSparkContext createSparkContext() {
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("Spark");
        sparkConf.setMaster("local[4]");

        return new JavaSparkContext(sparkConf);
    }

}
