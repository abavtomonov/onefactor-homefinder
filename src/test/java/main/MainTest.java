package main;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class MainTest {

    private static JavaSparkContext jsc;

    @BeforeClass
    public static void setUp() throws Exception {
        jsc = Main.createSparkContext();
    }

    @Test
    public void findCenter() throws Exception {
        JavaRDD<Vector> rdd = jsc.parallelize(Arrays.asList(Vectors.dense(0, 0), Vectors.dense(2, 4)));
        Vector center = Main.detectCenter(rdd);
        Assert.assertEquals(center.size(), 2);
        Assert.assertTrue(center.toArray()[0] == 1);
        Assert.assertTrue(center.toArray()[1] == 2);
    }

    @Test
    public void isItNightAtMoscowAt() throws Exception {
        Assert.assertEquals(Main.isItNightAtMoscowAt(createDateByHour(1)), true);
        Assert.assertEquals(Main.isItNightAtMoscowAt(createDateByHour(13)), false);
    }

    private Date createDateByHour(int hourOfDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, Calendar.JANUARY, 1, hourOfDay, 0);
        return calendar.getTime();
    }

}