package main;

import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DataEntryTest {
    @Test
    public void testCreate() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(1000 * 1000);
        Date testedDate = cal.getTime();

        DataEntry dataEntry = new DataEntry("123,1000,2,3");
        Assert.assertNotNull(dataEntry);
        assertEquals(dataEntry.getId(), 123);
        assertEquals(dataEntry.getDate(), testedDate);
        assertEquals(dataEntry.getX(), 2, 0);
        assertEquals(dataEntry.getY(), 3, 0);
    }
}